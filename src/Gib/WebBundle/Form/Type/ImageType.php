<?php

namespace Gib\WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

use Gib\WebBundle\Form\DataTransformer\GibUploadableFieldTransormer;

class ImageType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'compound' => false,
            //'data_class' => 'Symfony\Component\HttpFoundation\File\UploadedFile',
            'data_class' => null,
            'empty_data' => null,
            'multiple' => false,
            'required' => false,
            'mapped' => true,
            'shape' => null,
            'mWidth' => null
        ));
    }

    public function getParent()
    {
        return 'file';
    }

    public function getName()
    {
        return 'image';
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['shape'] = $options['shape'];
        $view->vars['mWidth'] = $options['mWidth'];
        //$view->vars['type'] = 'file';
    }

 /*   public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new GibUploadableFieldTransormer();


        $builder->add(
            $builder->create('file', 'text')
                ->addModelTransformer($transformer)
        );
    }*/
}
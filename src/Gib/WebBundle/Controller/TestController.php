<?php

namespace Gib\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityRepository;

/**
 * @Route("/test")
 */
class TestController extends Controller
{
    /**
     * @Route("/0", name="test0")
     * @Template()
     */
    public function test0Action()
    {
        $em = $this->getDoctrine()->getManager();

        $repo  = $em->getRepository('GibWebBundle:Article');

        $pieces = $repo->findAllUsedPieces();

        return array(
        	"pieces" => $pieces
        );
    }

    /**
     * @Route("/1/{pieceName}/{pieceKindName}", name="test1PieceNamePieceKindName")
     * @Template()
     */
    public function test1PieceNamePieceKindNameAction($pieceName, $pieceKindName)
    {
        $em = $this->getDoctrine()->getManager();

        $repo  = $em->getRepository('GibWebBundle:Article');

        $articles = $repo->findByPieceNameAndPieceKind($pieceName, $pieceKindName);

        return array(
            "articles" => $articles
        );
    }

}

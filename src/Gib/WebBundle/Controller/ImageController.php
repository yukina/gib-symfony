<?php

namespace Gib\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ImageController extends Controller
{

	/**
	 * @Route("/upl/t/img/NULL.png")
	 * @Route("/upl/s/img/NULL.png")
	 * @Route("/upl/n/img/NULL.png")
	 * @Route("/upl/b/img/NULL.png")
	 * @Route("/upl/img/NULL.png")
	 */
	public function nullAction()
	{
		header( 'Location: /img/NULL.png' );
		die();
	}

	/**
	 * @Route("/upl/{prefix}/{item}/{id}", requirements={"id" = ".+"})
	 */
	public function indexAction($prefix, $item, $id, Request $request)
	{
		/*$subfolder = "";

		$request = $this->getRequest();

		if ($request->query->has('thumb')) {
			$subfolder = "t";
		} else if ($request->query->has('small')) {
			$subfolder = "s";
		} else if ($request->query->has('normal')) {
			$subfolder = "n";
		} else if ($request->query->has('big')) {
			$subfolder = "b";
		}*/

		//if (strpos($this->getRequest()->server->get('HTTP_REFERER'), $this->getRequest()->server->get('HTTP_HOST')) === false) {
		    //exit();
		//}

		try {
			$filename = "upl/o/$item/$id";

			if (!file_exists($filename)) {
				$filename = "img/404.png";
			}

			/*if (strpos($this->getRequest()->server->get('HTTP_REFERER'), $this->getRequest()->server->get('HTTP_HOST')) === false) {
			    exit();
			}*/

			$response = new Response();

			// Set headers
			//$response->headers->set('Cache-Control', 'private');
			$response->headers->set('Content-type', mime_content_type($filename));
			$response->headers->set('Content-Disposition', 'inline; filename="' . basename($filename) . '"');
			$response->headers->set('Content-length', filesize($filename));

			// Send headers before outputting anything
			$response->sendHeaders();
			$response->setContent(readfile($filename));
		} catch (\Exception $e) {
			$filename = "img/404.png";

			$response = new Response();

			// Set headers
			//$response->headers->set('Cache-Control', 'private');
			$response->headers->set('Content-type', mime_content_type($filename));
			$response->headers->set('Content-Disposition', 'inline; filename="' . basename($filename) . '"');
			$response->headers->set('Content-length', filesize($filename));

			// Send headers before outputting anything
			$response->sendHeaders();

			$response->setContent(readfile($filename));
		}
		return $response;
	}
}
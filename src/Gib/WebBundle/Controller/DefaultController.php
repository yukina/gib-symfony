<?php

namespace Gib\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Doctrine\ORM\EntityRepository;

class DefaultController extends Controller
{

    /**
     * @Route("/catalogo/{pieceName}/{pieceKind}", name="catalogo", defaults={"pieceName" = "", "pieceKind" = ""})
     * @Template("GibWebBundle:Default:catalogo.html.twig")
     */
    public function catalogoAction($pieceName, $pieceKind) {
        // context = {
        //     "piecesNames": Piece.objects.all()[0].getAll(),
        //     "piecesKinds": PieceKind.objects.all(),
        // }
        $em = $this->getDoctrine()->getManager();

        $repo  = $em->getRepository('GibWebBundle:Article');

        $pieces = $repo->findAllUsedPieces();

        $articles = array();
        if ($pieceName || $pieceKind) 
            $articles = $repo->findByPieceNameAndPieceKind($pieceName, $pieceKind);

        return array(
            'piecesNames' => $pieces,
            'articles' => $articles,
        );
    }
}

<?php

namespace Gib\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/api/alloy")
 */
class AlloyController extends Controller
{
    /**
     * @Route("/")
     * @Method({"POST"})
     */
    public function postAction()
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $repo  = $em->getRepository('GibWebBundle:alloy');

            return new JsonResponse($repo->createArticle($em, $_POST)->raw());
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"This alloy has not been created. Maybe you are missing som params."));
        }
    }

    /**
     * @Route("/test")
     * @Method({"GET"})
     */
    public function testAction()
    {
        try {
            $em = $this->getDoctrine()->getManager();
            $repo  = $em->getRepository('GibWebBundle:alloy');

            $l = $repo->findOneById('5317931f206af');

            return new JsonResponse(array('yes' => 1, 'labor' => $l->getLabor()));
        } catch (\Exception $e) {
            return new JsonResponse(array('yes' => 0));
        }
    }

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function getAction()
    {
    	$em = $this->getDoctrine()->getManager();
        try {
     	    $alloys  = $em->getRepository('GibWebBundle:alloy')->findAllByFilter($_GET);

        	$ret = array();

            $fields = $this->getFields();
        	foreach ($alloys as $value) {
        		$ret[] = $value->raw($fields);
        	}
            return new JsonResponse($ret);
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/")
     * @Method({"PUT"})
     */
    public function putAction()
    {
        return new JsonResponse(array("error"=>"Method not allowed."));
    }

    /**
     * @Route("/")
     * @Method({"DELETE"})
     */
    public function deleteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $em->getRepository('GibWebBundle:alloy')->deleteAllByFilter($_GET);
        return new JsonResponse(array());
    }

    /**
     * @Route("/{id}")
     * @Method({"POST"})
     */
    public function postIdAction($id)
    {
        return new JsonResponse(array("error"=>"Method not allowed."));
    }

    /**
     * @Route("/{id}")
     * @Method({"GET"})
     */
    public function getIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $article  = $em->getRepository('GibWebBundle:alloy')->findOneById($id);

            if ($article === null) 
            {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $article->raw($this->getFields());
            return new JsonResponse($ret);
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/{id}")
     * @Method({"PUT"})
     */
    public function putIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $article  = $em->getRepository('GibWebBundle:alloy')->update($id, $_POST);


            if ($article === null) {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $article->raw();

            return new JsonResponse($ret);
        } catch (\Exception $e) {
            // 412
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/{id}")
     * @Method({"DELETE"})
     */
    public function deleteIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $article  = $em->getRepository('GibWebBundle:alloy')->remove($id);

            if ($article === null) {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $article->raw();

            return new JsonResponse($ret);
        } catch (\Exception $e) {
            // 412
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    public function getFields() {
        $fields = null;
        if (isset($_GET['_fields']) && $_GET['_fields']) {
            $fields = $_GET['_fields'];
        }
        return $fields;
    }
}

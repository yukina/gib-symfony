<?php

namespace Gib\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/api/piecekind")
 */
class PieceKindController extends Controller
{
    /**
     * @Route("/")
     * @Method({"POST"})
     */
    public function postAction(Request $request)
    {
            try {
        	$em = $this->getDoctrine()->getManager();
            $repo  = $em->getRepository('GibWebBundle:piecekind');

            return new JsonResponse($repo->createPieceKind($request)->raw());
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"This pieceKind has not been created. Maybe you are missing som params."));
        }
    }

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function getAction()
    {
    	$em = $this->getDoctrine()->getManager();
        try {
     	    $pieceKinds  = $em->getRepository('GibWebBundle:pieceKind')->findAllByFilter($_GET);

        	$ret = array();

            $fields = $this->getFields();
        	foreach ($pieceKinds as $value) {
        		$ret[] = $value->raw($fields);
        	}
            return new JsonResponse($ret);
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/")
     * @Method({"PUT"})
     */
    public function putAction()
    {
        return new JsonResponse(array("error"=>"Method not allowed."));
    }

    /**
     * @Route("/")
     * @Method({"DELETE"})
     */
    public function deleteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $em->getRepository('GibWebBundle:pieceKind')->deleteAllByFilter($_GET);
        return new JsonResponse(array());
    }

    /**
     * @Route("/{id}")
     * @Method({"POST"})
     */
    public function postIdAction($id)
    {
        return new JsonResponse(array("error"=>"Method not allowed."));
    }

    /**
     * @Route("/{id}", name="api_pieceKind_get_id")
     * @Method({"GET"})
     */
    public function getIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $pieceKind  = $em->getRepository('GibWebBundle:pieceKind')->findOneById($id);

            if ($pieceKind === null) 
            {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $pieceKind->raw($this->getFields());
            return new JsonResponse($ret);
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/{id}")
     * @Method({"PUT"})
     */
    public function putIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $pieceKind  = $em->getRepository('GibWebBundle:pieceKind')->update($id, $_POST);


            if ($pieceKind === null) {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $pieceKind->raw();

            return new JsonResponse($ret);
        } catch (\Exception $e) {
            // 412
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/{id}")
     * @Method({"DELETE"})
     */
    public function deleteIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $pieceKind  = $em->getRepository('GibWebBundle:pieceKind')->remove($id);

            if ($pieceKind === null) {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $pieceKind->raw();

            return new JsonResponse($ret);
        } catch (\Exception $e) {
            // 412
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    public function getFields() {
        $fields = null;
        if (isset($_GET['_fields']) && $_GET['_fields']) {
            $fields = $_GET['_fields'];
        }
        return $fields;
    }
}

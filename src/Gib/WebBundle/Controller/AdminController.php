<?php

namespace Gib\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/admin/2")
 */
class AdminController extends Controller
{
	/**
     * @Route("/")
     * @Method({"GET"})
     * @Template
     */
	public function indexAction()
    {
		return array();
	}

	/**
     * @Route("/piecekind")
     * @Method({"GET"})
     * @Template
     */
	public function piecekindAction() {
		$em = $this->getDoctrine()->getManager();
        $repo  = $em->getRepository('GibWebBundle:piecekind')->findAll();
		return array('piecekinds' => $repo);
	}

    /**
     * @Route("/fast")
     * @Method({"GET"})
     * @Template
     */
    public function piecekind2Action() {
        $em = $this->getDoctrine()->getManager();
        $repo  = $em->getRepository('GibWebBundle:stone')->findBy(array(), array('createdAt' => 'ASC'));

        echo "<pre>";
        foreach ($repo as $key => $value) {
            echo "'" . $value->getName() . "', ";
        }
        echo "</pre>";
        exit();
        return array('piecekinds' => $repo);
    }

	/**
     * @Route("/piecekind/new")
     * @Method({"GET"})
     * @Template
     */
	public function piecekindNewAction() {
		return array();
	}

	/**
	 * @Route("/refresh")
	 * @Method({"GET"})
	 * @Template
	 */
	public function refreshAction(Request $request)
    {

        $freeze = $request->query->get('freeze', 0);

        $au_new = -1;
        $ag_new = -1;
        $pt_new = -1;
        $pd_new = -1;

        if (!$freeze) {
    		$page = @file_get_contents('http://www.xmlcharts.com/cache/precious-metals.php?format=json');
            if ($page != false) {
        		$page = json_decode($page, true);
                $au_new = $page["eur"]["gold"];
                $ag_new = $page["eur"]["silver"];
                $pt_new = $page["eur"]["platinum"];
                $pd_new = $page["eur"]["palladium"];
            } else {
                $page = file_get_contents('http://ws.goldmoney.com/metal/prices/dataByTimeframe?type=spot&metal=pd&currency=eur&timeframe=1d&units=grams&precision=1&price=mid');
                $page = json_decode($page, true);
                $pd_new = $page["spotPrice"];

                $page = file_get_contents('http://ws.goldmoney.com/metal/prices/dataByTimeframe?type=spot&metal=pt&currency=eur&timeframe=1d&units=grams&precision=1&price=mid');
                $page = json_decode($page, true);
                $pt_new = $page["spotPrice"];

                $page = file_get_contents('http://ws.goldmoney.com/metal/prices/dataByTimeframe?type=spot&metal=au&currency=eur&timeframe=1d&units=grams&precision=1&price=mid');
                $page = json_decode($page, true);
                $au_new = $page["spotPrice"];

                $page = file_get_contents('http://ws.goldmoney.com/metal/prices/dataByTimeframe?type=spot&metal=ag&currency=eur&timeframe=1d&units=grams&precision=1&price=mid');
                $page = json_decode($page, true);
                $ag_new = $page["spotPrice"];
            }
        }

		$em = $this->getDoctrine()->getManager();
		$repo = $em->getRepository('GibWebBundle:symbol');

        $au = $repo->findOneBySymbol('AU');
        $au_old = $au->getPrice();
        $ag = $repo->findOneBySymbol('AG');
        $ag_old = $ag->getPrice();
        $pd = $repo->findOneBySymbol('PD');
        $pd_old = $pd->getPrice();
        $pt = $repo->findOneBySymbol('PT');
        $pt_old = $pt->getPrice();

        if ($freeze) {
            $au_new = $au_old;
            $ag_new = $ag_old;
            $pd_new = $pd_old;
            $pt_new = $pt_old;
        } else {
            $au->setPrice($au_new);
            $ag->setPrice($ag_new);
            $pt->setPrice($pt_new);
            $pd->setPrice($pd_new);

            $em->persist($au);
            $em->persist($ag);
            $em->persist($pd);
            $em->persist($pt);
            $em->flush();
        }

        /*
         * find cheaper ones
         */
        // Alloys
        $repo = $em->getRepository('GibWebBundle:Alloy');
        $alloys = $repo->findCheaper();

        // Articles
        $repo = $em->getRepository('GibWebBundle:Article');
        $articles = $repo->findCheaper($alloys);

        return array(
            'symbols' => array($au, $au_old, $ag, $ag_old, $pd, $pd_old, $pt, $pt_old),
            'alloys' => $alloys,
            'articles' => $articles
        );
	}

    /**
     * @Route("/article/reset/{id}")
     * @Method({"GET"})
     */
    public function resetPriceArticleAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('GibWebBundle:Article')->findOneById($id);
        $article->setPrice(0);
        $em->persist($article);
        $em->flush();

        header("Location: /admin/2/refresh?freeze=1");
        die();
    }

    /**
     * @Route("/article/disable/{id}")
     * @Method({"GET"})
     */
    public function disableArticleAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository('GibWebBundle:Article')->findOneById($id);
        $article->setEnabled(false);
        $em->persist($article);
        $em->flush();

        header("Location: /admin/2/refresh?freeze=1");
        die();
    }

    /**
     * @Route("/alloy/reset/{id}")
     * @Method({"GET"})
     */
    public function resetPriceAlloyAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $alloy = $em->getRepository('GibWebBundle:Alloy')->findOneById($id);
        $alloy->setPrice(0);
        $em->persist($alloy);
        $em->flush();

        header("Location: /admin/2/refresh?freeze=1");
        die();
    }

    /**
     * @Route("/alloy/disable/{id}")
     * @Method({"GET"})
     */
    public function disableAlloyAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $alloy = $em->getRepository('GibWebBundle:Alloy')->findOneById($id);
        $alloy->setEnabled(false);
        $em->persist($alloy);
        $em->flush();

        header("Location: /admin/2/refresh?freeze=1");
        die();
    }

    /**
     * @Route("/check_prices")
     * @Method({"GET"})
     * @Template
     */
    public function checkPricesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $alloys = $em->getRepository('GibWebBundle:alloy')->findAll();
        $articles = $em->getRepository('GibWebBundle:article')->findAll();

        $alertedAlloys = array();
        $alertedArticles = array();

        foreach ($alloys as $key => $alloy) {
            if ($alloy->getExpectedPrice() > $alloy->getPrice() and $alloy->getPrice() > 0 and $alloy->isAvailable()) {
                $alertedAlloys[] = $alloy;
            }
        }

        foreach ($articles as $key => $article) {
            if ($article->getExpectedPrice() > $article->getPrice() and $article->getPrice() > 0 and $alloy->isAvailable()) {
                $alertedArticles[] = $article;
            }
        }

        return array('alloys' => $alertedAlloys, "articles" => $alertedArticles);
    }
}










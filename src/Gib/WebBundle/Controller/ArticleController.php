<?php

namespace Gib\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/api/article")
 */
class ArticleController extends Controller
{
    /**
     * @Route("/")
     * @Method({"POST"})
     */
    public function postAction()
    {
        try {
        	$em = $this->getDoctrine()->getManager();
            $repo  = $em->getRepository('GibWebBundle:article');

            return new JsonResponse($repo->createArticle($em, $_POST)->raw());
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"This article has not been created. Maybe you are missing som params."));
        }
    }

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function getAction()
    {
    	$em = $this->getDoctrine()->getManager();
        try {
     	    $articles  = $em->getRepository('GibWebBundle:article')->findAllByFilter($_GET);

        	$ret = array();

            $fields = $this->getFields();
        	foreach ($articles as $value) {
        		$ret[] = $value->raw($fields);
        	}
            return new JsonResponse($ret);
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/")
     * @Method({"PUT"})
     */
    public function putAction()
    {
        return new JsonResponse(array("error"=>"Method not allowed."));
    }

    /**
     * @Route("/")
     * @Method({"DELETE"})
     */
    public function deleteAction()
    {
        $em = $this->getDoctrine()->getManager();
        $em->getRepository('GibWebBundle:article')->deleteAllByFilter($_GET);
        return new JsonResponse(array());
    }

    /**
     * @Route("/{id}")
     * @Method({"POST"})
     */
    public function postIdAction($id)
    {
        return new JsonResponse(array("error"=>"Method not allowed."));
    }

    /**
     * @Route("/{id}", name="api_article_get_id")
     * @Method({"GET"})
     */
    public function getIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $article  = $em->getRepository('GibWebBundle:article')->findOneById($id);

            if ($article === null)
            {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $article->raw($this->getFields());
            return new JsonResponse($ret);
        } catch (\Exception $e) {
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/{id}")
     * @Method({"PUT"})
     */
    public function putIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $article  = $em->getRepository('GibWebBundle:article')->update($id, $_POST);


            if ($article === null) {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $article->raw();

            return new JsonResponse($ret);
        } catch (\Exception $e) {
            // 412
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    /**
     * @Route("/{id}")
     * @Method({"DELETE"})
     */
    public function deleteIdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        try {
            $article  = $em->getRepository('GibWebBundle:article')->remove($id);

            if ($article === null) {
                return new JsonResponse(array("error"=>"this user not exists"));
            }

            $ret = $article->raw();

            return new JsonResponse($ret);
        } catch (\Exception $e) {
            // 412
            return new JsonResponse(array("error"=>"you are quering for fields that not exist"));
        }
    }

    public function getFields() {
        $fields = null;
        if (isset($_GET['_fields']) && $_GET['_fields']) {
            $fields = $_GET['_fields'];
        }
        return $fields;
    }
}

<?php

namespace Gib\WebBundle\Tests\Entity;

use Gib\WebBundle\Entity\Alloy;
use Gib\WebBundle\Entity\Symbol;
use Gib\WebBundle\Entity\AlloySymbol;
use \PHPUnit_Framework_TestCase;

class AlloyTest extends PHPUnit_Framework_TestCase
{
    public function testExpectedPrice()
    {
        $tin = new Symbol();
        $tin->setPrice(100);

        $copper = new Symbol();
        $copper->setPrice(60);

        $copperBronce = new AlloySymbol();
        $copperBronce->setSymbol($copper);
        $copperBronce->setQuantity(3);

        $tinBronce = new AlloySymbol();
        $tinBronce->setSymbol($tin);
        $tinBronce->setQuantity(1);

        $alloy = new Alloy();
        $alloy->setHours(20);
        $alloy->addSymbol($copperBronce);
        $alloy->addSymbol($tinBronce);
        $alloy->setPrice(100);

        $this->assertEquals(270, $alloy->getExpectedPrice());
        $this->assertEquals(70, $alloy->getRawPrice());
    }
}

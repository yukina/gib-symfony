<?php

namespace Gib\WebBundle\Tests\Entity;

use Gib\WebBundle\Entity\Alloy;
use Gib\WebBundle\Entity\Symbol;
use Gib\WebBundle\Entity\AlloySymbol;
use Gib\WebBundle\Entity\Gemstone;
use Gib\WebBundle\Entity\ArticleAlloy;
use Gib\WebBundle\Entity\ArticleGemstone;
use Gib\WebBundle\Entity\Article;

use \PHPUnit_Framework_TestCase;

class ArticleTest extends PHPUnit_Framework_TestCase
{
    public function testIndex()
    {
        $garnet = new Gemstone(); // 100
        $garnet->setPrice(100);

        $diamond = new Gemstone(); // 200
        $diamond->setPrice(200);

        $tin = new Symbol(); // 100
        $tin->setPrice(100);

        $copper = new Symbol(); // 60
        $copper->setPrice(60);

        $copperBronce = new AlloySymbol();
        $copperBronce->setSymbol($copper);
        $copperBronce->setQuantity(3);

        $tinBronce = new AlloySymbol();
        $tinBronce->setSymbol($tin);
        $tinBronce->setQuantity(1);

        $bronce = new Alloy(); 
        $bronce->sethours(17);
        $bronce->addSymbol($copperBronce);
        $bronce->addSymbol($tinBronce);
        $bronce->setPrice(230);
 
        $this->assertEquals(240, $bronce->getExpectedPrice());
        $this->assertEquals(70, $bronce->getRawPrice());

        $bronceRing = new ArticleAlloy();
        $bronceRing->setAlloy($bronce);
        $bronceRing->setQuantity(200);

        $garnetRing = new ArticleGemstone();
        $garnetRing->setGemstone($garnet);
        $garnetRing->setQuantity(6);

        $diamondRing = new ArticleGemstone();
        $diamondRing->setGemstone($diamond);
        $diamondRing->setQuantity(1);

        $ring = new Article(); // 400 + 14000 + 600 + 200 = 2600
        $ring->sethours(40);
        $ring->addAlloy($bronceRing);
        $ring->addGemstone($diamondRing);
        $ring->addGemstone($garnetRing);
        $ring->setPrice(47200);

        $this->assertEquals(47200, $ring->getPrice());
        $this->assertEquals(15200, $ring->getExpectedPrice());
    }
}

<?php
namespace Gib\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Gib\WebBundle\Form\Type\ImageType;

class ColourAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('cibjo')
                ->add('gia')
                ->add('scan')
            ->end()
            ->with('Image', array('class' => 'col-md-6'))
                ->add('file', 'image', array('data' => $this->getSubject()->getImage()))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('cibjo')
            ->add('gia')
            ->add('scan')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array()
                )
            ))
            ->add('image', 'string', array('template' => 'GibWebBundle:Admin:image_field.html.twig'))
            ->add('scan')
            ->add('cibjo')
            ->add('gia')
            ->add('createdAt')
        ;
    }

    public function prePersist($product) {
        $this->saveFile($product);
    }

    public function preUpdate($product)
    {
        if (isset($_POST['delete_image'])) {
            $product->setImage(null);
        } else {
            if ($product->getFile() !== null) {
                $this->saveFile($product);
            }
        }
    }

    public function saveFile($product)
    {
        $basepath = $this->getRequest()->getBasePath();
        $product->upload($basepath, "colors");
    }
}

<?php
namespace Gib\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Gib\WebBundle\Form\Type\ImageType;
use Gib\WebBundle\Entity\Article;

class ArticleAdmin extends Admin
{


    /* Create or UPdate view */
    protected function configureFormFields(FormMapper $formMapper)
    {
        //var_dump($old_image = $this->getSubject()->getImage());exit();
        $old_image = $this->getSubject()->getImage();
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('reference')
                ->add('name')
                ->add('description')
                ->add('price')
            ->end()
            ->with('Image', array('class' => 'col-md-6'))
                ->add('file', 'image', array('data' => $old_image))
            ->end()
            ->with('Building', array('class' => 'col-md-12'))

                ->add('piece', 'sonata_type_model_list', array(
                    'compound' => false,
                    'by_reference' => false,
                ))
                ->add('alloys', 'sonata_type_collection', array('by_reference' => false), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    //'data_class' => 'Gib\WebBundle\Entity\ArticleAlloy'
                ))
                ->add('gemstones', 'sonata_type_collection', array('by_reference' => false), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with('Security', array('class' => 'col-md-12'))
                ->add('enabled', null, array('required' => false))
            ->end()

        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagrid)
    {
        $datagrid
            ->add('reference')
            ->add('name')
            ->add('description')
            ->add('piece')
            ->add('alloys')
            ->add('gemstones')
            ->add('enabled')
            ->add('notAvailable', 'doctrine_orm_callback', array(
                'callback' => function($queryBuilder, $alias, $field, $value) {
                    if (!$value['value']) {
                        return;
                    }

                    $queryBuilder->leftJoin("s_alloys.alloy", 'al');
                    $queryBuilder->leftJoin("al.symbols", 'als');
                    $queryBuilder->leftJoin("als.symbol", 's');
                    $queryBuilder->orWhere("$alias.enabled = false");
                    $queryBuilder->orWhere("al.enabled = false");
                    $queryBuilder->orWhere("s.enabled = false");


                    return true;
                },
                'field_type' => 'checkbox',
            ))
        ;
    }

    // lista
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array()
                )
            ))
            ->add('reference')
            ->add('image', 'string', array('template' => 'GibWebBundle:Admin:image_field.html.twig'))
            ->add('name')
            ->add('price', 'string', array('template' => 'GibWebBundle:Admin:money_field.html.twig'))
            ->add('expectedPrice', 'string', array('template' => 'GibWebBundle:Admin:money_conditional_field.html.twig'))
            ->add('createdAt')
            ->add('available', 'boolean')
        ;
    }

    public function prePersist($product)
    {
        $this->saveFile($product);
    }

    public function preUpdate($product)
    {
        if (isset($_POST['delete_image'])) {
            $product->setImage(null);
        } else {
            if ($product->getFile() !== null) {
                $this->saveFile($product);
            }
        }
    }

    public function saveFile($product)
    {
        $basepath = $this->getRequest()->getBasePath();
        $product->upload($basepath, "articles");
    }
}

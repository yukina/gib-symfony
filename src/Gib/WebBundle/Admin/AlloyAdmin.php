<?php
namespace Gib\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Gib\WebBundle\Form\Type\ImageType;

class AlloyAdmin extends Admin
{
    private $container;


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('name')
                ->add('carat')
                ->add('description')
                ->add('price')
                ->add('hours')
            ->end()
            ->with('Image', array('class' => 'col-md-6'))
                ->add('file', 'image', array('data' => $this->getSubject()->getImage()))
            ->end()
            ->with('Symbols', array('class' => 'col-md-12'))
                ->add('symbols', 'sonata_type_collection', array('by_reference' => false), array(
                    'edit' => 'inline',
                    'inline' => 'table',
                ))
            ->end()
            ->with('Security', array('class' => 'col-md-12'))
                ->add('enabled', 'checkbox', array('required' => false))
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('carat')
            ->add('description')
            ->add('symbols')
            ->add('articles')
            ->add('price')
            ->add('hours')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array()
                )
            ))
            ->add('image', 'string', array('template' => 'GibWebBundle:Admin:image_field.html.twig'))
            ->add('name')
            ->add('carat')
            ->add('price', 'string', array('template' => 'GibWebBundle:Admin:money_field_kilo.html.twig'))
            ->add('expectedPrice', 'string', array('template' => 'GibWebBundle:Admin:money_conditional_field_kilo.html.twig'))
            ->add('createdAt')
            ->add('available', 'boolean')
            ;
    }


    public function prePersist($product) {
        $this->saveFile($product);
    }

    public function preUpdate($product)
    {
        if (isset($_POST['delete_image'])) {
            $product->setImage(null);
        } else {
            if ($product->getFile() !== null) {
                $this->saveFile($product);
            }
        }
    }

    public function saveFile($product)
    {
        $basepath = $this->getRequest()->getBasePath();
        $product->upload($basepath, "alloys");
    }
}

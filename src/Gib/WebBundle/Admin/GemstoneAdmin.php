<?php
namespace Gib\WebBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Gib\WebBundle\Form\Type\ImageType;

class GemstoneAdmin extends Admin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', array('class' => 'col-md-6'))
                ->add('name')
                ->add('price')
                ->add('weight')
                ->add('size')
            ->end()
            ->with('Image', array('class' => 'col-md-6'))
                ->add('file', 'image', array('data' => $this->getSubject()->getImage()))
            ->end()
            ->with('Building', array('class' => 'col-md-12'))
                ->add('stone', 'sonata_type_model_list', array(
                        'compound' => false,
                        'by_reference' => false
                    ))
                ->add('shape', 'sonata_type_model_list', array(
                        'compound' => false,
                        'by_reference' => false
                    ))
                ->add('cut', 'sonata_type_model_list', array(
                        'compound' => false,
                        'by_reference' => false,
                        'required' => false
                    ))
                ->add('colour', 'sonata_type_model_list', array(
                        'compound' => false,
                        'by_reference' => false
                    ))
                ->add('purity', 'sonata_type_model_list', array(
                        'compound' => false,
                        'by_reference' => false
                    ))
            ->end()
            ->with('Security', array('class' => 'col-md-12'))
                ->add('enabled')
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('stone')
            ->add('shape')
            ->add('cut')
            ->add('colour')
            ->add('purity')
            ->add('price')
            ->add('weight')
            ->add('size')
            ->add('enabled')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('_action', 'actions', array(
                'actions' => array(
                    'edit' => array()
                )
            ))
            ->add('image', 'string', array('template' => 'GibWebBundle:Admin:image_field.html.twig'))
            ->add('name')
            ->add('stone')
            ->add('shape')
            ->add('cut')
            ->add('colour')
            ->add('purity')
            ->add('price')
            ->add('createdAt')
            ->add('enabled')
            ;
    }

    public function prePersist($product) {
        $this->saveFile($product);
    }

    public function preUpdate($product)
    {
        if (isset($_POST['delete_image'])) {
            $product->setImage(null);
        } else {
            if ($product->getFile() !== null) {
                $this->saveFile($product);
            }
        }
    }

    public function saveFile($product)
    {
        $basepath = $this->getRequest()->getBasePath();
        $product->upload($basepath, "gemstones");
    }
}

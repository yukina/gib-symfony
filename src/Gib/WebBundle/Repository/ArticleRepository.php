<?php
namespace Gib\WebBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Gib\WebBundle\Repository\Common\CustomRepository;

class ArticleRepository extends CustomRepository
{
    /*
        basicmanete hay varios piece con el mismo nombre, por eso solo pedimos el nombre, yo ya me acuerdo, 
        luego por cada nombre de pieza hay un array de piecekinds basicamente por que ahi hay mas info (por si hace falta)

        plan B
        crear una tabla PieceName
     */
    public function findAllUsedPieces()
    {
        $articles = $this->createQueryBuilder('a')
                        ->getQuery()
                        ->getResult();

        $ret = array();
        foreach ($articles as $articleKey => $article) {
            $piece = $article->getPiece();
            if ($piece) {
                if (!isset($ret[$piece->getName()])) {
                    $ret[$piece->getName()] = array();
                }

                $pieceKinds = $piece->getPieceKinds();
                foreach ($pieceKinds as $key => $pieceKind) {
                    $ret[$piece->getName()][$pieceKind->getName()] = $pieceKind;
                }
            }
        }
        return $ret;
    }

    /*
     * Retorna todos los articulos de un determinado tipo de pieza, como la pieza no la tienes, se le pasara un String "Anillo",...
     * Para ser constantes con PieceKind se hara lo mismo.
     *
     * @param String $pieceName         Nombre de la pieza (Anillo, Brazalete, ...)
     * @param String $pieceKindName     Nombre del tipo de la pieza (Liso, ...)
     * @param int    $offset            Pagina de la qual se quiere la consulta. Por defecto es 0
     * @param int    $limit             Tamaño de cada una de las páginas. Por defecto es 25
     */
    public function findByPieceNameAndPieceKind($pieceName, $pieceKindName, $offset=0, $limit=25)
    {

        $query =
          'SELECT a
           FROM GibWebBundle:Article a INNER JOIN a.piece p INNER JOIN p.pieceKinds pk
           WHERE pk.name = :pieceKindName AND
                 p.name = :pieceName';
        $articles = $this->getEntityManager()
                    ->createQuery($query)
                    ->setParameter('pieceName', $pieceName)
                    ->setParameter('pieceKindName', $pieceKindName)
                    ->getResult();
        return $articles;
    }

    /** esta funcion hace la misma consulta sql que findALlUsedPieces */
    public function test()
    {
        $articles = $this->createQueryBuilder('a')
                    ->getQuery()
                    ->getResult();
        return $articles;
    }


    //
    public function findAllByFilter($params=null)
    {
        if ($params === null || sizeof($params) === 0) {
            return $this->findAll();
        } else {
            $qb = $this->createQueryBuilder('p');
            foreach ($params as $key => $value) {
                if ($key[0] !== "_") {
                    $qb = $qb->where("p.$key = '$value'");
                }
            }
            return $qb->getQuery()->getResult();
        }
    }

    public function createArticle($request)
    {
        $article = new PieceKind();
        $article->setName($request->request->get('name'));
        $article->setDescription($request->request->get('description'));
        $article->setPrice($request->request->get('price'));
        $article->setRealPrice($request->request->get('realPrice'));
        $article->setHours($request->request->get('hours'));
        foreach($request->files as $uploadedFile) {
            $article->setImage($uploadedFile);
        }
        $article->upload($request->getBasePath());
        $em = $this->getEntityManager();
        $em->persist($article);
        $em->flush();

        return $article;
    }

    public function findCheaper($alloys)
    {
        $query =
            "SELECT a
             FROM GibWebBundle:Article a
             WHERE a.enabled = 1";

        $allArticles = $this->getEntityManager()
                       ->createQuery($query)
                       ->getResult();

        $articles = array();

        foreach ($allArticles as $article) {
            if ($article->getExpectedPrice() > $article->getPrice() && $article->getPrice() > 0) {
                $articles[] = $article;
            }
        }
        return $articles;
    }
}
<?php
namespace Gib\WebBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Gib\WebBundle\Entity\PieceKind;
use Gib\WebBundle\Repository\Common\CustomRepository;

class PieceKindRepository extends CustomRepository
{

	/*
	 returns a dicctionary foreach piece all piecekinds it have
	 */
	public function findAllWithAnArticle()
	{
		$pieces = $this->createQueryBuilder('p')
					->orderBy('name')
					->getQuery()
					->getResult();
		return $pieces;
	}


	///////

	public function findAllByFilter($params=null)
	{
		if ($params === null || sizeof($params) === 0) {
			return $this->findAll();
		} else {
			$qb = $this->createQueryBuilder('p');
			foreach ($params as $key => $value) {
				if ($key[0] !== "_") {
					$qb = $qb->where("p.$key = '$value'");
				}
			}
			return $qb->getQuery()->getResult();
		}
	}

	public function createPieceKind($request)
	{
		$pieceKind = new PieceKind();
		$pieceKind->setName($request->request->get('name'));
	 	foreach($request->files as $uploadedFile) {
		    $pieceKind->setImage($uploadedFile);
		}
		$pieceKind->upload($request->getBasePath());
	 	$em = $this->getEntityManager();
	    $em->persist($pieceKind);
	    $em->flush();

		return $pieceKind;
	}
}
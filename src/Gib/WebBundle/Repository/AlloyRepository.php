<?php
namespace Gib\WebBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Gib\WebBundle\Repository\Common\CustomRepository;

class AlloyRepository extends CustomRepository {


    public function findAllUsedPieces()
    {
        $articles = $this->createQueryBuilder('a')
                        ->getQuery()
                        ->getResult();

        $ret = array();
        foreach ($articles as $articleKey => $article) {
            $piece = $article->getPiece();
            if (!isset($ret[$piece->getName()])) {
                $ret[$piece->getName()] = array();
            }

            $pieceKinds = $piece->getPieceKinds();
            foreach ($pieceKinds as $key => $pieceKind) {
                $ret[$piece->getName()][$pieceKind->getName()] = $pieceKind;
            }
        }
        return $ret;
    }

    public function findCheaper()
    {
        $query =
            "SELECT a
             FROM GibWebBundle:Alloy a
             WHERE a.enabled = 1 AND EXISTS (
                SELECT s
                FROM GibWebBundle:Symbol s, GibWebBundle:AlloySymbol sa
                WHERE a.id = sa.alloy AND s.id = sa.symbol AND s.symbol IN ('AU', 'AG', 'PT', 'PD')
             )";
        $allAlloys = $this->getEntityManager()
                       ->createQuery($query)
                       ->getResult();

        $alloys = array();

        foreach ($allAlloys as $alloy) {
            if ($alloy->getExpectedPrice() > $alloy->getPrice() && $alloy->getPrice() > 0) {
                $alloys[] = $alloy;
            }
        }
        return $alloys;
    }
}
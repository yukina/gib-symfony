<?php
namespace Gib\WebBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Gib\WebBundle\Repository\Common\CustomRepository;

class PieceRepository extends CustomRepository
{
	/*
	 returns a dicctionary foreach piece all piecekinds it have
	 */
	public function findByNameAndPieceKindName($pieceName, $pieceKindName)
	{
		$piecesKinds = $this->getEntityManager()->getRepository('GibWebBundle:PieceKind')->findByName($pieceKindName);

		// $articles = $this->createQueryBuilder('a')
  //                       ->in("a.piece", $pieces)
  //                       ->getQuery()
  //                       ->getResult();

		$pieces = $this->createQueryBuilder('p')
		    ->where('p.name = :pieceName')
		    ->exists("p.piecekind", ":piecesKinds")
		    ->setParameter('pieceName', $pieceName)
		    ->setParameter('pieceKinds', $pieceKinds)
		    ->getQuery()
		    ->getResult();
		return $pieces;
	}

	/*
	{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery('SELECT p FROM AcmeStoreBundle:Product p ORDER BY p.name ASC')
            ->getResult();
    }
    */
}
<?php

namespace Gib\WebBundle\Repository\Common;

use Doctrine\ORM\EntityRepository;

abstract class CustomRepository extends EntityRepository
{


	protected function getAllowedOperations() {
		return array(
			'=' => 1,
			'<' => 1,
			'<=' => 1,
			'<>' => 1,
			'>' => 1,
			'>=' => 1,
			'!=' => 1,
			'LIKE' => 1
		);
	}
	public function findByFilter($limit = 5, $offset = 0, $orderBy = false, $inverseOrder=false, $filter=array())
	{

		$qb = $this->createQueryBuilder('t');

		$operators = $this->getAllowedOperations();

		$first = true;

		foreach ($filter as $key => $value) {

			$subQuery = explode('/', $value);

			$reference = $subQuery[0];
			if (count($subQuery) > 1) {
				$operator = $subQuery[1];

				if (!isset($operators[$operator])) {
					return array(
						'error' => "$operator is not valid"
						);
				}
			} else {
				$operator = '=';
			}

			$subQuery = "t.$key $operator '$reference'";

			if ($first) {
				$qb = $qb->where($subQuery);
			} else {
				$qb = $qb->andWhere($subQuery);
			}
		}

		$qb = $qb->setMaxResults($limit)
        	->setFirstResult($offset);

        if ($orderBy) {
			$qb = $qb->orderBy("t.$orderBy", $inverseOrder?'DESC':'ASC');
        }
        return 	$qb->getQuery()
			->getResult();
	}
}
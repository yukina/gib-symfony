(function() {

    if (window.Navi) {
        console.error('var window.Navi is already in use.');
    }

    var initialBody;
    var initialTitle = document.title;
    var isInitial = true;

    /**
     * cada elemento de historial tendra una id y content
     */
    var historial = {};

    var Navi = function() {

        var that = this;

        this.init = function() {

            var opts;
            var successCallback;
            var failureCallback;

            var currentRequest;

            /*
             *  options : {
             *      + id:           string
             *      + path:         string
             *      - replace:      boolean     false
             *      - method:       string      "POST"
             *      - headers:      array       []
             *      - title:        string 
             *      - params
             *  }
             */
            this.to = function(options, onSuccess, onFailure, onProgress) {
                if (!options.id) {
                    console.error('options.id must be defined');
                    return;
                }

                if (!options.path) {
                    console.error('options.path must be defined');
                    return;
                }

                // TODO si hay alguna petiocion primero hay que aprarla
                opts = {};
                opts.id = options.id;
                opts.path = options.path;
                opts.replace = options.replace || false;
                opts.method = options.method || "POST";
                opts.headers = options.headers || [];
                opts.headers["X-Requested-With"] = "XMLHttpRequest";
                opts.params = options.params;
                onSuccess = onSuccess || function() {return true;};
                onFailure = onFailure || function() {return true;};
                onProgress = onProgress || function() {return true;};

                var dom = document.getElementById(opts.id);
                if (!dom) {
                    console.error("Not exist a dom element with id = " + opts.id + " .");
                    return;
                }

                var resp = currentRequest && currentRequest.abort();

                currentRequest = new XMLHttpRequest();

                currentRequest.onprogress = function(event) {
                    if (onProgress(event, currentRequest) !== false) {
                        return;
                    }
                    that.onprogress && that.onprogress(event, currentRequest);
                };

                currentRequest.onreadystatechange = function() {
                    if (currentRequest.readyState == 4) {
                        if (currentRequest.status >= 200 && currentRequest.status < 400) {
                            _onSuccess(opts, currentRequest, onSuccess);
                        } else {
                            _onFailure(opts, currentRequest, onFailure);
                        }
                    }
                };

                currentRequest.open(opts.method, opts.path, true);

                for (var header in opts.headers) {
                    currentRequest.setRequestHeader(header, opts.headers[header]);
                }

                currentRequest.send(opts.params);
            };

            /*
             * calling callbacks
             */
            var _onSuccess = function(options, request, cb) {
                var parser = new DOMParser();
                var parsedDocument = parser.parseFromString(request.response, "text/xml").children[0];
                var newElement = findOneByAttr(parsedDocument, 'id', options.id);

                if (cb(request, newElement) === false) {
                    return;
                }

                if (parsedDocument.getElementsByTagName('parsererror')[0]) {
                    console.error("html received has errors", parsedDocument);
                    newElement = parsedDocument.getElementsByTagName('parsererror')[0];
                }

                var serializer = new XMLSerializer();

                var text = serializer.serializeToString(newElement);

                options.content = text;
                options.title = parsedDocument.getElementsByTagName('title')[0].textContent;
                if (options.replace) {
                    history.replaceState(JSON.stringify(options), options.title, options.path);
                } else {
                    history.pushState(JSON.stringify(options), options.title, options.path);
                }
                document.getElementById(options.id).outerHTML = text;

                historial[options.path] = {
                    id: options.id,
                    content: text
                };

                that.onload && that.onload(document.getElementById(options.id));
                that.load(document.getElementById(options.id));

            };

            var _onFailure = function(options, request, cb) {
                if (cb(request, newElement) === false) {
                    return;
                }
                that.onfailure && that.onfailure(document.getElementById(options.id));
            };


            function attachListener(element) {
                var method = method || 'GET';
                return function(event) {
                    event.preventDefault();

                    that.to({
                        id : element.dataset.navi,
                        path : element.href,
                        method : element.dataset.method,
                        title : element.dataset.title,
                        params : element.dataset.params
                    });

                    return false;
                };
            }

            this.load = function(dom) {
                dom = dom || document.body;
                
                var list = dom.getElementsByTagName('a');

                for (var i = 0; i < list.length; i++) {
                    if (list[i].dataset && list[i].dataset.navi) {
                        list[i].addEventListener('click', attachListener(list[i]));
                    }
                }

            };
        };

        this.init();
    };

    function findOneByAttr(xmlDoc, attr, val) {
        if (xmlDoc.getAttribute(attr) == val) {
            return xmlDoc;
        }

        for (var i = 0; i < xmlDoc.children.length; i++) {
            var ret = findOneByAttr(xmlDoc.children[i], attr, val);
            if (ret) {
                return ret;
            }
        }

        return null;
    }
    window.onpopstate = function(event) {
        if (!history.state) {
            if (!isInitial && initialBody) {
                document.body.outerHTML = initialBody;
                Navi.onload && Navi.onload(initialBody);
                event.preventDefault();
            } else if (!isInitial && !initialBody) {
                location.href = location.href;
            } else if (isInitial) {
                initialBody = document.body.outerHTML;
                event.preventDefault();
            }
            isInitial = false;
            return false;
        } else {
            var state = JSON.parse(history.state);

            if (location.href != state.path) {
                document.title = state.title;
                document.getElementById(state.id).outerHTML = state.content;
            }
            event.preventDefault();

            return false;
        }

    };

    
    Object.defineProperty(window, "Navi", {
        enumerable: true,
        writable: false,
        value: new Navi()
    });

    window.addEventListener('load', function() {
        if (navigator.userAgent.indexOf("Opera") >= 0) {
            return;
        }
        window.Navi.load();
    });
})();

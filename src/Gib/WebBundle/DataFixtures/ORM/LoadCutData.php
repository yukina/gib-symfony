<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Cut;

class LoadCutData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $names = ['Bouquette', 'Briallance', 'Faro', 'Brilliant', 'Briolette', 'Cabochón', 'Anillo de Torus', 'Concavo'];

        foreach ($names as $key => $name) {

            $id = $key + 1;
            $item = new Cut();
            $item->setName($name);

            $manager->persist($item);
            //echo ($key + 1) . "\n";

            $this->addReference("cu_$id", $item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
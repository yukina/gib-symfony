<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Symbol;

class LoadSymbolData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $loadImages = true;
        $names = ['Oro', 'Plata', 'Platino', 'Paladio', 'Cobre', 'Aluminio', 'Hierro', 'Zinc', 'Níquel', 'Cadmio'];
        $symbols = ['AU', 'AG', 'PT', 'PD', 'CU', 'AL', 'FE', 'ZN', 'NI', 'CD'];
        $densities = [19320, 10490, 21450, 12023, 8960, 2700, 7870, 7130, 8900,8650];
        $prices = [0, 0, 0, 0, 0.0050607, 0.0016135, 0.0001209032, 0.001659328, 0.0137537928, 0.003608];

        foreach ($names as $key => $value) {

            $id = $key + 1;

            $item = new Symbol();
            $item->setId($id);
            $item->setName($value);
            $item->setDescription('');
            $item->setSymbol($symbols[$key]);
            $item->setDensity($densities[$key]);
            $item->setPrice($prices[$key]);
            $item->setImage("/symbols/$id.png");
            $item->setEnabled(true);

            $manager->persist($item);

            if ($loadImages) {
                $item->generateFiles("symbols", $id, "png");
            }

            $this->addReference("sy_$id", $item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Config;

class LoadConfigData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $config = new Config();
        $config->setName('token');
        $config->setValue(uniqid());

        $manager->persist($config);
        $manager->flush();
    }
}
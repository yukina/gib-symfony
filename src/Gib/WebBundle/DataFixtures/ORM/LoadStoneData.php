<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Stone;

class LoadStoneData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        $loadImages = true;

        $names = ['Ágata', 'Amazonita', 'Ambar', 'Amatista', 'Ametrino', 'Andalucita', 'Andesina', 'Apatita', 'Aguamarina', 'Aragonita', 'Venturina', 'Berilio', 'Sanguinaria', 'Broncita', 'Cornalina', 'Calcedonia', 'Charoite', 'Crisoprasa', 'Citrino', 'Coral', 'Perla Cultivada', 'Diópsido', 'Esmeralda', 'Perla cultivada en agua dulce', 'Granate', 'Howlite', 'Iolite', 'Jaspe', 'Kunzite', 'Cianita', 'Labradorita', 'Lapislazuli', 'Larimar', 'Larvikite', 'Magnesita', 'Malaquita', 'Marcasita', 'Mookaite', 'Piedra Lunar', 'Obsidiana', 'Ónix', 'Ópalo', 'Peridoto', 'Prehnite', 'Pirita', 'Cuarzo', 'Cuarcita', 'Rodocrosita', 'Rodonita', 'Rubelita', 'Rubí', 'Rutilo', 'Zafiro', 'Ónice', 'Seraphinite', 'Serpentina', 'Sodalita', 'Espinela', 'Piedra del Sol', 'Tanzanita', 'Ojo del Tigre', 'Topacio', 'Turmalina', 'Turquesa', 'Unakite', 'Zoisita', 'Diamond'];

        foreach ($names as $key => $name) {

            $id = $key + 1;
            $item = new Stone();
            $item->setId($id);
            $item->setName($name);
            $item->setImage("/stones/$id.png");
            $manager->persist($item);
            //echo ($key + 1) . "\n";

            if ($loadImages) {
                $item->generateFiles("stones", $id, "png");
            }

            $this->addReference("st_$id", $item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
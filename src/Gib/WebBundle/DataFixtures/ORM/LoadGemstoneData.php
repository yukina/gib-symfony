<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Gemstone;

class LoadGemstoneData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $loadImage = true;
        // stone, shape, color, cut, price, size, weight, name
        $items = [
            //['st_', 'sh_', 'c_', 'cu_6', 0.00, "_________________", 0.00, '___CT ___'],
                ['st_46', 'sh_18', 'c_20', 'cu_3', 13.92, "12.00mm x 12.00mm", 6.15, '6.15CT Cuarzo rosado'],
                ['st_46', 'sh_18', 'c_21', 'cu_3', 13.91, "12.00mm x 12.00mm", 6.15, '6.15CT Cuarzo ahumado'],
                ['st_46', 'sh_18', 'c_22', 'cu_3', 13.91, "12.00mm x 12.00mm", 6.15, '6.15CT Cuarzo limón'],
                ['st_46', 'sh_5', 'c_22', 'cu_1', 21.95, "16.00mm x 16.00mm", 12.37, '12.37CT Cuarzo limón'],
                ['st_46', 'sh_5', 'c_21', 'cu_2', 18.97, "15.00mm x 15.00mm", 10.06, '10.06CT Cuarzo ahumado'],
                ['st_9', 'sh_18', 'c_18', 'cu_4', 28.35, "8.00mm x 8.00mm", 1.92, '1.92CT Aguamarina'],
                ['st_63', 'sh_20', 'c_19', 'cu_4', 39.15, "6.00mm x 8.00mm", 1.21, '1.21CT Turmalina'],
                ['st_25', 'sh_12', 'c_19', 'cu_4', 43.20, "6.00mm x 8.00mm", 0.66, '0.66CT Granate de Tanzania'],
                ['st_59', 'sh_14', 'c_21', 'cu_4', 49.95, "6.00mm x 8.00mm", 0.66, '1.40CT Piedra solar de Shingaya'],
                ['st_62', 'sh_12', 'c_18', 'cu_4', 52.65, "6.00mm x 9.00mm", 1.35, '1.35CT Topacio azul'],
                ['st_60', 'sh_20', 'c_18', 'cu_4', 41.85, "4.00mm x 5.00mm", 0.38, '0.38CT Tanzanita'],
                ['st_4', 'sh_6', 'c_23', 'cu_4', 47.25, "6.00mm x 12.00mm", 1.62, '1.62CT Amatista'],
                ['st_29', 'sh_4', 'c_20', 'cu_4', 56.75, "7.50mm x 13.00mm", 5.18, '5.18CT Kunzite'],
                ['st_62', 'sh_19', 'c_24', 'cu_4', 109.35, "12.00mm x 12.00mm", 5.95, '5.95CT Topacio místico'],
                ['st_9', 'sh_1', 'c_18', 'cu_4', 31.77, "4.00mm x 6.00mm", 0.52, '0.52CT Aguamarina'],
                ['st_62', 'sh_5', 'c_25', 'cu_4', 55.35, "10.00mm x 10.00mm", 4.00, '4.00CT Topacio'],
                ['st_43', 'sh_14', 'c_19', 'cu_4', 51.30, "7.75mm x 7.75mm", 1.58, '1.58CT Peridot'],
                ['st_46', 'sh_5', 'c_20', 'cu_5', 125.55, "19.00mm x 19.00mm", 16.92, '16.92CT Cuarzo Rosado'],
                ['st_10', 'sh_12', 'c_18', 'cu_5', 17.83, "10.00mm x 13.00mm", 5.30, '5.30CT Aragonita Azul'],
                ['st_4', 'sh_20', 'c_23', 'cu_5', 63.45, "10.00mm x 12.00mm", 3.65, '3.65CT Amatista'],
                ['st_4', 'sh_18', 'c_23', 'cu_5', 16.96, "12.00mm x 12.00mm", 5.25, '5.25CT Amatista'],
                ['st_43', 'sh_16', 'c_19', 'cu_5', 44.55, "7.00mm x 9.00mm", 2.46, '2.46CT Peridot'],
                ['st_10', 'sh_18', 'c_25', 'cu_5', 30.89, "14.00mm x 14.00mm", 9.70, '9.70CT Aragonita'],
                ['st_63', 'sh_15', 'c_20', 'cu_5', 100.70, "6.00mm x 6.00mm", 1.08, '1.08CT Turmalina rosa'],
                ['st_46', 'sh_5', 'c_24', 'cu_5', 30.20, "15.00mm x 15.00mm", 9.70, '9.70CT Cuarzo'],
                ['st_50', 'sh_14', 'c_20', 'cu_6', 114.93, "13.50mm x 13.50mm", 7.55, '7.55CT Rubalita'],
                ['st_22', 'sh_15', 'c_26', 'cu_6', 36.45, "6.00mm x 6.00mm", 1.45, '1.45CT Diopsido'],
                ['st_25', 'sh_8', 'c_25', 'cu_6', 13.50, "6.00mm x 6.00mm", 1.00, '1.00CT Granate'],
                ['st_39', 'sh_5', 'c_24', 'cu_6', 60.75, "9.50mm x 9.50mm", 4.05, '4.05CT Piedra lunar'],
                ['st_39', 'sh_12', 'c_24', 'cu_6', 64.80, "5.50mm x 8.50mm", 1.16, '1.16CT Piedra lunar'],
                ['st_23', 'sh_19', 'c_19', 'cu_6', 89.10, "5.75mm x 5.75mm", 0.71, '0.71CT Esmeralda'],
                ['st_49', 'sh_6', 'c_20', 'cu_6', 73.72, "7.00mm x 14.00mm", 4.32, '4.32CT Rodonita'],
                ['st_7', 'sh_14', 'c_19', 'cu_6', 44.55, "6.00mm x 6.00mm", 0.69, '0.69CT Andesina'],
                ['st_53', 'sh_20', 'c_18', 'cu_7', 6984.90, "10.60mm x 16.34mm", 7.86, '7.86CT Zafiro'],
                ['st_59', 'sh_14', 'c_21', 'cu_7', 4500.90, "18.00mm x 18.00mm", 7.91, '7.91CT Piedra solar'],
                ['st_63', 'sh_20', 'c_24', 'cu_7', 4453.65, "6.25mm x 7.30mm", 0.87, '0.87CT Turmalina'],
                ['st_60', 'sh_20', 'c_18', 'cu_7', 3111.47, "10.50mm x 14.60mm", 7.19, '7.19CT Tanzanita'],
                ['st_9', 'sh_20', 'c_18', 'cu_7', 4035.15, "12.00mm x 20.00mm", 7.29, '7.29CT Aguamarina'],
                ['st_46', 'sh_20', 'c_22', 'cu_8', 4306.50, "16.00mm x 22.00mm", 20.50, '20.50CT Cuarzo limón'],
        ];

        foreach ($items as $key => $value) {
            $id = $key + 1;

            $item = new Gemstone();
            $item->setId($id);
            $item->setStone($this->getReference($value[0]));
            $item->setShape($this->getReference($value[1]));
            $item->setColour($this->getReference($value[2]));
            $item->setPurity($this->getReference("p_5"));
            if ($value[3]) {
                $item->setCut($this->getReference($value[3]));
            }
            $item->setPrice($value[4]);
            $item->setSize($value[5]);
            $item->setWeight($value[6]);
            $item->setName($value[7]);
            $item->setImage("/gemstones/$id.jpg");
            $item->setEnabled(true);

            $manager->persist($item);

            if ($loadImage) {
                $item->generateFiles("gemstones", $id, "jpg");
            }

            $this->addReference("g_$id", $item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
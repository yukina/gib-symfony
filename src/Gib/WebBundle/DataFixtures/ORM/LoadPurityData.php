<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Purity;

class LoadPurityData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $loadImages = true;
        $names = ['FL', 'IF', 'VVS1', 'VVS2', 'VS1', 'VS2', 'SI1', 'SI2', 'I1', 'I2', 'I3'];

        foreach ($names as $key => $name) {

            $id = $key + 1;
            $item = new Purity();
            $item->setId($id);
            $item->setName($name);
            $item->setImage("/purities/$id.png");
            $manager->persist($item);

            if ($loadImages) {
                $item->generateFiles("purities", $id, "png");
            }

            $this->addReference("p_$id", $item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
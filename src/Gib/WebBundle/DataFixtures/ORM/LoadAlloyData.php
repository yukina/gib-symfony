<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Alloy;
use Gib\WebBundle\Entity\AlloySymbol;

class LoadAlloyData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        //Oro amarillo: Aleación que tiene, por cada 1000 g de la misma, 750 g de oro fino, 125 g de plata fina y 125 g de cobre.
        $id = 1;
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Amarillo');
        $item->setCarat(18);
        $item->setDescription("Oro amarillo: Por cada 1000 g hay 750 g de oro fino, 125 g de plata fina y 125 g de cobre.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(125);

        $as3 = new AlloySymbol();
        $as3->setAlloy($item);
        $as3->setSymbol($this->getReference("sy_5"));
        $as3->setQuantity(125);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);
        $manager->persist($as3);

        $this->addReference("al_$id", $item);

        $id++;


        // Oro rojo: Aquí las proporciones son 750 g de oro fino y 250 g de cobre.
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Rojo');
        $item->setCarat(18);
        $item->setDescription("Oro rojo: Por cada 1000 g hay 750 g de oro fino y 250 g de cobre.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as3 = new AlloySymbol();
        $as3->setAlloy($item);
        $as3->setSymbol($this->getReference("sy_5"));
        $as3->setQuantity(250);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as3);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro rosa: El contenido de 1000 g presenta 750 g de oro fino 50 g de plata fina y 200 g de cobre.
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Rosa');
        $item->setCarat(18);
        $item->setDescription("Oro rosa: Por cada 1000 g hay 750 g de oro fino 50 g de plata fina y 200 g de cobre.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(50);

        $as3 = new AlloySymbol();
        $as3->setAlloy($item);
        $as3->setSymbol($this->getReference("sy_5"));
        $as3->setQuantity(200);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);
        $manager->persist($as3);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro blanco o Paladio: Por cada 1000 g de oro blanco o paladio hay 750 g de oro fino y de 100 a 160 g de paladio. El resto es de plata fina.
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Blanco');
        $item->setCarat(18);
        $item->setDescription("Oro blanco o Paladio: Por cada 1000 g hay 750 g de oro fino, 160 g de paladio y 90 g de plata fina.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(90);

        $as4 = new AlloySymbol();
        $as4->setAlloy($item);
        $as4->setSymbol($this->getReference("sy_4"));
        $as4->setQuantity(160);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);
        $manager->persist($as4);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro blanco o Paladio: Por cada 1000 g de oro blanco o paladio hay 750 g de oro fino y de 100 a 160 g de paladio. El resto es de plata fina.
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Blanco 2');
        $item->setCarat(18);
        $item->setDescription("Oro blanco o Paladio: Por cada 1000 g hay 750 g de oro fino, 100 g de paladio y 150 g de plata fina.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(150);

        $as4 = new AlloySymbol();
        $as4->setAlloy($item);
        $as4->setSymbol($this->getReference("sy_4"));
        $as4->setQuantity(100);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);
        $manager->persist($as4);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro Blanco
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Blanco 3');
        $item->setCarat(18);
        $item->setDescription("75% oro, 25% platino");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as3 = new AlloySymbol();
        $as3->setAlloy($item);
        $as3->setSymbol($this->getReference("sy_3"));
        $as3->setQuantity(222.5);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as3);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro Blanco
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Blanco 4');
        $item->setCarat(18);
        $item->setDescription("75% oro, 25% paladio");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as4 = new AlloySymbol();
        $as4->setAlloy($item);
        $as4->setSymbol($this->getReference("sy_4"));
        $as4->setQuantity(222.5);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as4);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro Blanco
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Blanco 5');
        $item->setCarat(18);
        $item->setDescription("75% oro, 10% paladio, 10% niquel, 5% Zinc");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as4 = new AlloySymbol();
        $as4->setAlloy($item);
        $as4->setSymbol($this->getReference("sy_4"));
        $as4->setQuantity(100);

        $as8 = new AlloySymbol();
        $as8->setAlloy($item);
        $as8->setSymbol($this->getReference("sy_8"));
        $as8->setQuantity(50);

        $as9 = new AlloySymbol();
        $as9->setAlloy($item);
        $as9->setSymbol($this->getReference("sy_9"));
        $as9->setQuantity(100);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as4);
        $manager->persist($as8);
        $manager->persist($as9);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro gris: En 1000 g de oro gris hay 750 g de oro fino y alrededor de 150 g de níquel. El resto es de cobre.
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Gris');
        $item->setCarat(18);
        $item->setDescription("Oro gris: En 1000 g hay 750 g de oro fino y alrededor de 150 g de níquel. El resto es de cobre.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as5 = new AlloySymbol();
        $as5->setAlloy($item);
        $as5->setSymbol($this->getReference("sy_5"));
        $as5->setQuantity(100);

        $as9 = new AlloySymbol();
        $as9->setAlloy($item);
        $as9->setSymbol($this->getReference("sy_9"));
        $as9->setQuantity(100);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as5);
        $manager->persist($as9);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro verde: En 1000 g de oro verde hay 750 g de oro fino y 250 g de plata.
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Verde o Electrum');
        $item->setCarat(18);
        $item->setDescription("Oro verde o Electrum: En 1000 g hay 750 g de oro fino y 250 g de plata.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(250);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro azul: En 1000 g de oro azul hay 750 g de oro fino y 250 g de hierro.
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Azul');
        $item->setCarat(18);
        $item->setDescription("Oro azul: En 1000 g hay 750 g de oro fino y 250 g de hierro.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as7 = new AlloySymbol();
        $as7->setAlloy($item);
        $as7->setSymbol($this->getReference("sy_7"));
        $as7->setQuantity(250);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as7);

        $this->addReference("al_$id", $item);
        $id++;


        // Hoja muerta
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Hoja muerta');
        $item->setCarat(18);
        $item->setDescription("Hoja muerta: En 1000 g hay 700 g de oro fino y 300 de plata fina.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(700);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(300);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);

        $this->addReference("al_$id", $item);
        $id++;

        // Oro verde agua
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Verde Agua');
        $item->setCarat(18);
        $item->setDescription("Oro Verde Agua: En 1000 g hay 600 g de oro fino y 400 de plata fina.");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(600);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(400);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);

        $this->addReference("al_$id", $item);
        $id++;
        $manager->flush();


        // Oro Rosado
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Rosado');
        $item->setCarat(18);
        $item->setDescription("75% oro, 22.25% cobre, 2,75% plata");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(222.5);

        $as5 = new AlloySymbol();
        $as5->setAlloy($item);
        $as5->setSymbol($this->getReference("sy_5"));
        $as5->setQuantity(27.5);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);
        $manager->persist($as5);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro GRis claro
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Gris Claro');
        $item->setCarat(18);
        $item->setDescription("75% oro, 17% hierro, 8% Cobre");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as5 = new AlloySymbol();
        $as5->setAlloy($item);
        $as5->setSymbol($this->getReference("sy_5"));
        $as5->setQuantity(80);

        $as7 = new AlloySymbol();
        $as7->setAlloy($item);
        $as7->setSymbol($this->getReference("sy_7"));
        $as7->setQuantity(170);


        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as5);
        $manager->persist($as7);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro Verde Claro
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Verde Claro');
        $item->setCarat(18);
        $item->setDescription("75% oro, 23% cobre, 2% Cadmio");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as5 = new AlloySymbol();
        $as5->setAlloy($item);
        $as5->setSymbol($this->getReference("sy_5"));
        $as5->setQuantity(230);

        $as10 = new AlloySymbol();
        $as10->setAlloy($item);
        $as10->setSymbol($this->getReference("sy_10"));
        $as10->setQuantity(20);


        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as5);
        $manager->persist($as10);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro Verde
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Verde');
        $item->setCarat(18);
        $item->setDescription("75% oro, 20% plata, 5% Cobre");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(200);

        $as5 = new AlloySymbol();
        $as5->setAlloy($item);
        $as5->setSymbol($this->getReference("sy_5"));
        $as5->setQuantity(50);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);
        $manager->persist($as5);

        $this->addReference("al_$id", $item);
        $id++;


        // 10 cadmio
        // Oro Verde Oscuro
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Verde Oscuro');
        $item->setCarat(18);
        $item->setDescription("75% oro, 15% plata, 6% Cobre, 4% Cadmio");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(750);

        $as2 = new AlloySymbol();
        $as2->setAlloy($item);
        $as2->setSymbol($this->getReference("sy_2"));
        $as2->setQuantity(150);

        $as5 = new AlloySymbol();
        $as5->setAlloy($item);
        $as5->setSymbol($this->getReference("sy_5"));
        $as5->setQuantity(60);

        $as10 = new AlloySymbol();
        $as10->setAlloy($item);
        $as10->setSymbol($this->getReference("sy_10"));
        $as10->setQuantity(40);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as2);
        $manager->persist($as5);
        $manager->persist($as10);

        $this->addReference("al_$id", $item);
        $id++;


        // Oro Purpura
        $item = new Alloy();
        $item->setId($id);
        $item->setReference($id);
        $item->setName('Oro Purpura');
        $item->setCarat(18);
        $item->setDescription("80% oro, 20% Aluminio");
        $item->setEnabled(true);

        $as1 = new AlloySymbol();
        $as1->setAlloy($item);
        $as1->setSymbol($this->getReference("sy_1"));
        $as1->setQuantity(800);

        $as6 = new AlloySymbol();
        $as6->setAlloy($item);
        $as6->setSymbol($this->getReference("sy_6"));
        $as6->setQuantity(200);

        $manager->persist($item);
        $manager->persist($as1);
        $manager->persist($as6);

        $this->addReference("al_$id", $item);
        $id++;


        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Colour;

class LoadColorData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $loadImages = true;

        $ids = ["D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T"];
        $cibjos = ["Blanco Excepcional", "Blanco Excepcional", "Blanco Extra", "Blanco Extra", "Blanco", 'Blanco Tenue', 'Blanco Tenue', 'Ligero Color', 'Ligero Color', 'Color', 'Color', 'Color', 'Color', 'Color', 'Color', 'Color', 'Color'];
        $gias = ["River", "River", "Top Wesselton", "Top Wesselton", "Wesselton", "Top Crystal", "Crystal", "Top Cape", "Top Cape", "Cape", "Cape", "Yellow", "Yellow", "Yellow", "Yellow", "Yellow", "Yellow"];

        foreach ($ids as $key => $value) {

            $id = $key + 1;
            $item = new Colour();
            $item->setId($value);
            $item->setCibjo($cibjos[$key]);
            $item->setGia($gias[$key]);
            $item->setScan($value);
            $item->setImage("/colors/$value.png");

            $manager->persist($item);

            if ($loadImages) {
                $item->generateFiles("colors", $value, "png");
            }

            $this->addReference("c_$id", $item);
        }

        // 18

        $colours = ['Azul', 'Verde', 'Rosa', 'Marrón', 'Amarillo', 'Púrpura', 'Blanco', 'Rojo', 'Negro', 'Gris'];

        foreach ($colours as $key => $value) {

            $id = $key + 18;
            $item = new Colour();
            $item->setId($id);
            $item->setCibjo($value);
            $item->setGia($value);
            $item->setScan($value);

            $manager->persist($item);

            $this->addReference("c_$id", $item);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
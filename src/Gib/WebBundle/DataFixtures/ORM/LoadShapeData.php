<?php
namespace Gib\WebBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Gib\WebBundle\Entity\Shape;

class LoadShapeData extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $loadImages = true;
        $shapes = ['Esmeralda', 'Cuadrado (Lozenge)', 'Epaulette', 'Gota', 'Redondo', 'Marqués', false, 'Cabujón Redondo', 'Cabujón Oval', false, false, 'Pera', 'Briolette', 'Trillante', 'Cuadrado', 'Baguette', 'Baguette Ahusado', 'Cojín Antigua', 'Corazón', 'Oval', false, 'Hexagono'];

        foreach ($shapes as $key => $shape) {

            if ($shape) {
                $id = $key + 1;
                $item = new Shape();
                $item->setId($id);
                $item->setName($shape);
                $item->setImage("/shapes/$id.png");
                $manager->persist($item);

                if ($loadImages) {
                    $item->generateFiles("shapes", $id, "png");
                }
                $this->addReference("sh_$id", $item);
            }
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
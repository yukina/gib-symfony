<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Gib\WebBundle\Repository\PieceKindRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="piecekind")
 */
class PieceKind extends ImageEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    public function __construct() {
    }

    public function __toString() {
        return "$this->name";
    }

    /**
     * @ORM\PrePersist()
     */
    public function preSave() {
        $this->generateId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PieceKind
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return PieceKind
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return PieceKind
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return PieceKind
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() 
    {
        $this->generateId();
        return $this->id;
    }

    public function generateId()
    {
        if ($this->id === null) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function generateFilename()
    {
        return $this->generateId();
    }

}
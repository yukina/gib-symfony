<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class ImageEntity
{
    /**
     * @Assert\File(maxSize="8000000")
     */
    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public abstract function generateFilename();
    public abstract function setImage($image);

    public function upload($basepath, $folder)
    {
        if (null === $this->getFile()) {
            return;
        }

        if (null === $basepath) {
            return;
        }

        $name = $this->generateFilename();
        $extension = $this->getFile()->guessExtension();
        $path = "o/$folder/". $name . "." . $extension;

        $this->getFile()->move($basepath . "upl/o/$folder", $name . "." . $extension);
        $this->setImage("/$folder/$name.$extension");

        $image = imagecreatefromstring(file_get_contents("upl/$path"));
        imagecolortransparent($image, imagecolorallocatealpha($image, 0, 0, 0, 127));
        list($width, $height) = getimagesize("upl/$path");

        $this->convert("upl", 114, 64, "t", $width, $height, $path, $folder, $name, $extension, $image);
        $this->convert("upl", 456, 256, "s", $width, $height, $path, $folder, $name, $extension, $image);
        $this->convert("upl", 1280, 720, "n", $width, $height, $path, $folder, $name, $extension, $image);
        $this->convert("upl", 1920, 1080, "b", $width, $height, $path, $folder, $name, $extension, $image);

        imagedestroy($image);
    }

    public function generateFiles($folder, $name, $extension) {

        $base = getcwd();

        if (!file_exists ("$base/web/upl")) {
            system("mkdir $base/web/upl");
        }

        if (!file_exists ("$base/web/upl/o")) {
            system("mkdir $base/web/upl/o");
            system("cp -R ext-files/o/$folder web/upl/o/$folder");
        }

        if (!file_exists ("$base/web/upl/o/$folder")) {
            system("mkdir $base/web/upl/o/$folder");
        }

        system("cp -R ext-files/o/$folder/$name.$extension web/upl/o/$folder/");

        $path = "o/$folder/$name.$extension";
        $image = imagecreatefromstring(file_get_contents("web/upl/$path"));
        imagecolortransparent($image, imagecolorallocatealpha($image, 0, 0, 0, 127));
        list($width, $height) = getimagesize("web/upl/$path");

        $this->convert("web/upl", 114, 64, "t", $width, $height, $path, $folder, $name, $extension, $image);
        $this->convert("web/upl", 456, 256, "s", $width, $height, $path, $folder, $name, $extension, $image);
        $this->convert("web/upl", 1280, 720, "n", $width, $height, $path, $folder, $name, $extension, $image);
        $this->convert("web/upl", 1920, 1080, "b", $width, $height, $path, $folder, $name, $extension, $image);
    }

    private function convert($root, $MX, $MY, $PREFIX, $width, $height, $path, $folder, $name, $extension, $image=null) {
        $h = $MY;
        $w = ($h * $width) / $height;

        $base = getcwd();

        if (!file_exists ("$base/$root")) {
            system("mkdir $base/$root");
        }

        if (!file_exists ("$base/$root/$PREFIX")) {
            system("mkdir $base/$root/$PREFIX");
        }

        if (!file_exists ("$base/$root/$PREFIX/$folder")) {
            system("mkdir $base/$root/$PREFIX/$folder");
        }

        if ($MY >= $height) {
            if (file_exists ("$base/$root/$PREFIX/$folder/$name.$extension")) {
                system("rm $base/$root/$PREFIX/$folder/$name.$extension");
            }
            //system("rm $base/$root/$PREFIX/$folder/$name.$extension");
            system("ln -s $base/$root/$path $base/$root/$PREFIX/$folder/$name.$extension");
        } else {
            if (file_exists ("$base/$root/$PREFIX/$folder/$name.$extension")) {
                system("rm $base/$root/$PREFIX/$folder/$name.$extension");
            }
            system("convert -quality 60 -size $width" . "x$height $root/$path -resize $w" . "x$h $root/$PREFIX/$folder/$name.$extension");
        }
    }

}
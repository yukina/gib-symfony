<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="Gib\WebBundle\Repository\ArticleRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="article")
 * @Gedmo\Uploadable(path="/img/alloy", filenameGenerator="SHA1", allowOverwrite=false, appendNumber=true)
 */
class Article extends ImageEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @ORM\ManyToOne(targetEntity="Piece")
     * @ORM\JoinColumn(name="piece_id", referencedColumnName="id")
     */
    protected $piece;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="float")
     */
    protected $hours;

    /**
     * @ORM\OneToMany(targetEntity="ArticleAlloy", mappedBy="article", cascade={"all"}, orphanRemoval=true)
     */
    protected $alloys;

    /**
     * @ORM\OneToMany(targetEntity="ArticleGemstone", mappedBy="article", cascade={"all"}, orphanRemoval=true)
     */
    protected $gemstones;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $enabled;

    private $expectedPrice = null;
    private $available = null;

    public function __construct() {
        $this->description = "";
        $this->alloys = new ArrayCollection();
        $this->gemstones = new ArrayCollection();
        $this->price = 0;
        $this->hours = 1;
        $this->enabled = false;
        $this->reference = 0;
    }

    public function __toString() {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Article
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Article
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    public function getLabor()
    {
        return Parameter::$LABOR;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function getExpectedPrice()
    {
        if ($this->expectedPrice === null) {
            $this->available = $this->enabled;
            $this->expectedPrice = 0;

            foreach ($this->alloys as $alloy) {
                if ($alloy->getAlloy() != null) {
                    $this->expectedPrice += $alloy->getQuantity() * $alloy->getAlloy()->getRawPrice();
                    $this->available &= $alloy->getAlloy()->isAvailable();
                }
            }

            foreach ($this->gemstones as $gemstone) {
                if ($gemstone->getGemstone() != null) {
                    $this->expectedPrice += $gemstone->getQuantity() * $gemstone->getGemstone()->getPrice();
                    $this->available &= $gemstone->getGemstone()->isEnabled();

                }
            }

            $this->expectedPrice += $this->hours * $this->getLabor();
            $this->expectedPrice = $this->expectedPrice;

            $this->available = (bool)$this->available;
        }

        return $this->expectedPrice;
    }

    /**
     * Add alloys
     *
     * @param \Gib\WebBundle\Entity\ArticleAlloy $alloys
     * @return Article
     */
    public function addAlloy(\Gib\WebBundle\Entity\ArticleAlloy $alloy)
    {
        $alloy->setArticle($this);
        $this->alloys[] = $alloy;
        return $this;
    }

    /**
     * Remove alloys
     *
     * @param \Gib\WebBundle\Entity\ArticleAlloy $alloys
     */
    public function removeAlloy(\Gib\WebBundle\Entity\ArticleAlloy $alloys)
    {
        $this->alloys->removeElement($alloys);
    }

    /**
     * Get alloys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlloys()
    {
        return $this->alloys;
    }

    /**
     * Add gemstones
     *
     * @param \Gib\WebBundle\Entity\ArticleGemstone $gemstones
     * @return Article
     */
    public function addGemstone(\Gib\WebBundle\Entity\ArticleGemstone $gemstone)
    {
        $gemstone->setArticle($this);
        $this->gemstones[] = $gemstone;
        return $this;
    }

    /**
     * Remove gemstones
     *
     * @param \Gib\WebBundle\Entity\ArticleGemstone $gemstones
     */
    public function removeGemstone(\Gib\WebBundle\Entity\ArticleGemstone $gemstones)
    {
        $this->gemstones->removeElement($gemstones);
    }

    /**
     * Get gemstones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGemstones()
    {
        return $this->gemstones;
    }

    /**
     * Set piece
     *
     * @param \Gib\WebBundle\Entity\Piece $piece
     * @return Article
     */
    public function setPiece(\Gib\WebBundle\Entity\Piece $piece = null)
    {
        $this->piece = $piece;

        return $this;
    }

    /**
     * Get piece
     *
     * @return \Gib\WebBundle\Entity\Piece
     */
    public function getPiece()
    {
        return $this->piece;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Article
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set hours
     *
     * @param float $hours
     * @return Article
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return float
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Article
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function isAvailable()
    {
        $this->getExpectedPrice();

        return $this->available;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->generateId();
        return $this->id;
    }

    public function generateId()
    {
        if ($this->id === null) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function generateFilename() {
        return $this->generateId();
    }

    /**
     * Set reference
     *
     * @param integer $reference
     * @return Article
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return integer
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if ($this->reference == 0) {
            $this->reference = $this->id;
        }
    }
}
<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="alloy_symbol")
 * @ORM\HasLifecycleCallbacks
 */
class AlloySymbol
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Alloy", inversedBy="symbols")
     * @ORM\JoinColumn(name="alloy_id", referencedColumnName="id", nullable=false)
     */
    protected $alloy;

    /**
     * @ORM\ManyToOne(targetEntity="Symbol", inversedBy="alloys")
     * @ORM\JoinColumn(name="symbol_id", referencedColumnName="id", nullable=false)
     */
    protected $symbol;

    /**
     * @ORM\Column(type="float")
     */
    protected $quantity;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    public function __construct() {
        $this->hours = 1;
    }

    public function __toString() {
        return "$this->alloy ($this->symbol)";
    }

    /**
     * @ORM\PrePersist()
     */
    public function preSave() {
        $this->generateId();
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Alloy_Symbol
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return Alloy_Symbol
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set alloy
     *
     * @param \Gib\WebBundle\Entity\Alloy $alloy
     * @return Alloy_Symbol
     */
    public function setAlloy($alloy = null)
    {
        $this->alloy = $alloy;


        // if (isset($symbol)) {
        //     $this->name .= " [$symbol->getName()]";
        // }

        return $this;
    }

    /**
     * Get alloy
     *
     * @return \Gib\WebBundle\Entity\Alloy
     */
    public function getAlloy()
    {
        return $this->alloy;
    }

    /**
     * Set symbol
     *
     * @param \Gib\WebBundle\Entity\Symbol $symbol
     * @return Alloy_Symbol
     */
    public function setSymbol($symbol = null)
    {
        $this->symbol = $symbol;

        // if (isset($alloy)) {
        //     $this->name .= "$alloy->getName() [$symbol->getName()]";
        // }
        return $this;
    }

    /**
     * Get symbol
     *
     * @return \Gib\WebBundle\Entity\Symbol
     */
    public function getSymbol()
    {
        return $this->symbol;
    }


    /**
     * Set price
     *
     * @param float $price
     * @return AlloySymbol
     */
    public function setPrice($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    private function generateId() {
        if ($this->id === null) {
            $this->id = uniqid();
            //$this->id = bin2hex(openssl_random_pseudo_bytes(12));
        }
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return AlloySymbol
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return AlloySymbol
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return AlloySymbol
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
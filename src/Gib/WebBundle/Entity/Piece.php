<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Gib\WebBundle\Repository\PieceRepository")
 * @ORM\Table(name="piece")
 * @ORM\HasLifecycleCallbacks
 */
class Piece extends ImageEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", length=25)
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="PieceKind")
     * @ORM\JoinTable(name="piece_piecekind",
     *      joinColumns={@ORM\JoinColumn(name="piece_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="piecekind_id", referencedColumnName="id")}
     * )
     */
    protected $pieceKinds;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    public function __construct() {
        $this->pieceKinds = new ArrayCollection();
    }

    public function __toString() {
        return $this->name . " " . join(' ', $this->pieceKinds->toArray());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Piece
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add pieceKinds
     *
     * @param \Gib\WebBundle\Entity\PieceKind $pieceKinds
     * @return Piece
     */
    public function addPieceKind(\Gib\WebBundle\Entity\PieceKind $pieceKinds)
    {
        $this->pieceKinds[] = $pieceKinds;

        return $this;
    }

    /**
     * Remove pieceKinds
     *
     * @param \Gib\WebBundle\Entity\PieceKind $pieceKinds
     */
    public function removePieceKind(\Gib\WebBundle\Entity\PieceKind $pieceKinds)
    {
        $this->pieceKinds->removeSymbol($pieceKinds);
    }

    /**
     * Get pieceKinds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPieceKinds()
    {
        return $this->pieceKinds;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Piece
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Piece
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Piece
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->generateId();
        return $this->id;
    }

    public function generateId()
    {
        if ($this->id === null) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function generateFilename()
    {
        return $this->generateId();
    }

    /**
     * Set reference
     *
     * @param integer $reference
     * @return Piece
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return integer
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if ($this->reference == 0) {
            $this->reference = $this->id;
        }
    }
}
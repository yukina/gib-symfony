<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="symbol")
 */
class Symbol extends ImageEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="string", length=3, unique=true)
     */
    protected $symbol;

    /**
     * @ORM\Column(type="float")
     */
    protected $density;

    /**
     * €/g
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\OneToMany(targetEntity="AlloySymbol", mappedBy="symbol")
     */
    protected $alloys;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $enabled;

    public function __construct() {
        $this->description = "";
        $this->alloys = new ArrayCollection();
    }

    public function __toString() {
        return "$this->name [$this->symbol]";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Symbol
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Symbol
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     * @return Symbol
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set density
     *
     * @param float $density
     * @return Symbol
     */
    public function setDensity($density)
    {
        $this->density = $density;

        return $this;
    }

    /**
     * Get density
     *
     * @return float
     */
    public function getDensity()
    {
        return $this->density;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Symbol
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add alloys
     *
     * @param \Gib\WebBundle\Entity\Alloy_Symbol $alloys
     * @return Symbol
     */
    public function addAlloy($alloys)
    {
        $this->alloys[] = $alloys;

        return $this;
    }

    /**
     * Remove alloys
     *
     * @param \Gib\WebBundle\Entity\Alloy_Symbol $alloys
     */
    public function removeAlloy($alloys)
    {
        $this->alloys->removeSymbol($alloys);
    }

    /**
     * Get alloys
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlloys()
    {
        return $this->alloys;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Symbol
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Symbol
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Symbol
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isEnabled() {
        return $this->enabled;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->generateId();
        return $this->id;
    }

    public function generateId()
    {
        if ($this->id === null) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function generateFilename()
    {
        return $this->generateId();
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
}
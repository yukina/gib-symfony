<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="color")
 */
class Colour extends ImageEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    protected $cibjo;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $scan;

    /**
     * @ORM\Column(type="string", length=25)
     */
    protected $gia;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    public function __construct() {
    }

    public function __toString() {
        return "$this->scan - $this->gia - $this->cibjo";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCibjo() 
    {
        return $this->cibjo;
    }

    public function setCibjo($cibjo)
    {
        $this->cibjo = $cibjo;
        return $this;
    }

    public function getScan() 
    {
        return $this->scan;
    }

    public function setScan($scan)
    {
        $this->scan = $scan;
        return $this;
    }

    public function getGia() 
    {
        return $this->gia;
    }

    public function setGia($gia)
    {
        $this->gia = $gia;
        return $this;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Colour
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return Colour
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return Colour
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Colour
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Colour
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist() 
    {
        $this->generateId();
        return $this->id;
    }

    public function generateId()
    {
        if ($this->id === null) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function generateFilename()
    {
        return $this->generateId();
    }
}
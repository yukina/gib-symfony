<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="article_gemstone")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt")
 */
class ArticleGemstone
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="gemstones")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", nullable=false)
     */
    protected $article;

    /**
     * @ORM\ManyToOne(targetEntity="Gemstone", inversedBy="articles")
     * @ORM\JoinColumn(name="gemstone_id", referencedColumnName="id", nullable=false)
     */
    protected $gemstone;

    /**
     * @ORM\Column(type="float")
     */
    protected $quantity;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    public function __construct() {
    }

    public function __toString() 
    {
        //FIXME
        return "";
        return "$this->article ($this->gemstone)";
    }

    /**
     * @ORM\PrePersist()
     */
    public function preSave() {
        $this->generateId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return ArticleGemstone
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set article
     *
     * @param \Gib\WebBundle\Entity\Article $article
     * @return ArticleGemstone
     */
    public function setArticle(\Gib\WebBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Gib\WebBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set gemstone
     *
     * @param \Gib\WebBundle\Entity\Gemstone $gemstone
     * @return ArticleGemstone
     */
    public function setGemstone(\Gib\WebBundle\Entity\Gemstone $gemstone = null)
    {
        $this->gemstone = $gemstone;

        return $this;
    }

    /**
     * Get gemstone
     *
     * @return \Gib\WebBundle\Entity\Gemstone 
     */
    public function getGemstone()
    {
        return $this->gemstone;
    }

    private function generateId() {
        if ($this->id === null) {
            $this->id = uniqid();
            //$this->id = bin2hex(openssl_random_pseudo_bytes(12));
        }
    }

    /**
     * Set id
     *
     * @param string $id
     * @return ArticleGemstone
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ArticleGemstone
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return ArticleGemstone
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    
        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return ArticleGemstone
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}
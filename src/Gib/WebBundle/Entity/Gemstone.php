<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="gemstone")
 */
class Gemstone extends ImageEntity
{

    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", length=64, unique=true)
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Shape")
     * @ORM\JoinColumn(name="shape_id", referencedColumnName="id")
     */
    protected $shape;

    /**
     * @ORM\ManyToOne(targetEntity="Cut")
     * @ORM\JoinColumn(name="cut_id", referencedColumnName="id", nullable=true)
     */
    protected $cut;

    /**
     * @ORM\ManyToOne(targetEntity="Colour")
     * @ORM\JoinColumn(name="colour_id", referencedColumnName="id")
     */
    protected $colour;

    /**
     * @ORM\ManyToOne(targetEntity="Purity")
     * @ORM\JoinColumn(name="purity_id", referencedColumnName="id")
     */
    protected $purity;

    /**
     * @ORM\ManyToOne(targetEntity="Stone")
     * @ORM\JoinColumn(name="stone_id", referencedColumnName="id")
     */
    protected $stone;

    /**
     * @ORM\Column(type="string", length=25)
     */
    protected $size;

    /**
     * @ORM\Column(type="float")
     */
    protected $weight;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\OneToMany(targetEntity="ArticleGemstone", mappedBy="gemstone")
     */
    protected $articles;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $enabled;

    public function __construct() {
        $this->articles = new ArrayCollection();
        $this->enabled = false;
    }

    public function __toString() {
        return "$this->name ($this->colour, $this->purity, $this->shape, $this->size)";
    }

    /**
     * @ORM\PrePersist()
     */
    public function preSave() {
        $this->generateId();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Gemstone
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set shape
     *
     * @param \Gib\WebBundle\Entity\Shape $shape
     * @return Gemstone
     */
    public function setShape(\Gib\WebBundle\Entity\Shape $shape = null)
    {
        $this->shape = $shape;

        return $this;
    }

    /**
     * Get shape
     *
     * @return \Gib\WebBundle\Entity\Shape
     */
    public function getShape()
    {
        return $this->shape;
    }

    /**
     * Set colour
     *
     * @param \Gib\WebBundle\Entity\Colour $colour
     * @return Gemstone
     */
    public function setColour(\Gib\WebBundle\Entity\Colour $colour = null)
    {
        $this->colour = $colour;

        return $this;
    }

    /**
     * Get colour
     *
     * @return \Gib\WebBundle\Entity\Colour
     */
    public function getColour()
    {
        return $this->colour;
    }

    /**
     * Set purity
     *
     * @param \Gib\WebBundle\Entity\Purity $purity
     * @return Gemstone
     */
    public function setPurity(\Gib\WebBundle\Entity\Purity $purity = null)
    {
        $this->purity = $purity;

        return $this;
    }

    /**
     * Get purity
     *
     * @return \Gib\WebBundle\Entity\Purity
     */
    public function getPurity()
    {
        return $this->purity;
    }

    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set getE
     *
     * @param float $price
     * @return Alloy
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Alloy
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get Weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Add articles
     *
     * @param \Gib\WebBundle\Entity\ArticleGemstone $articles
     * @return Gemstone
     */
    public function addArticle(\Gib\WebBundle\Entity\ArticleGemstone $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param \Gib\WebBundle\Entity\ArticleGemstone $articles
     */
    public function removeArticle(\Gib\WebBundle\Entity\ArticleGemstone $articles)
    {
        $this->articles->removeSymbol($articles);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Gemstone
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Gemstone
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Gemstone
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getExpectedPrice() {
        return $this->price;

    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isEnabled() {
        return $this->enabled;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->generateId();
        return $this->id;
    }

    public function generateId()
    {
        if ($this->id === null) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function generateFilename()
    {
        return $this->generateId();
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set reference
     *
     * @param integer $reference
     * @return Gemstone
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return integer
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if ($this->reference == 0) {
            $this->reference = $this->id;
        }
    }

    /**
     * Set stone
     *
     * @param \Gib\WebBundle\Entity\Stone $stone
     * @return Gemstone
     */
    public function setStone(\Gib\WebBundle\Entity\Stone $stone = null)
    {
        $this->stone = $stone;
    
        return $this;
    }

    /**
     * Get stone
     *
     * @return \Gib\WebBundle\Entity\Stone 
     */
    public function getStone()
    {
        return $this->stone;
    }

    /**
     * Set cut
     *
     * @param \Gib\WebBundle\Entity\Cut $cut
     * @return Gemstone
     */
    public function setCut(\Gib\WebBundle\Entity\Cut $cut = null)
    {
        $this->cut = $cut;
    
        return $this;
    }

    /**
     * Get cut
     *
     * @return \Gib\WebBundle\Entity\Cut 
     */
    public function getCut()
    {
        return $this->cut;
    }
}
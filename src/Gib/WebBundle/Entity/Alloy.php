<?php
namespace Gib\WebBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="Gib\WebBundle\Repository\AlloyRepository")
 * @ORM\Table(name="alloy")
 * @ORM\HasLifecycleCallbacks
 */
class Alloy extends ImageEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="AlloySymbol", mappedBy="alloy", cascade={"all"})
     */
    protected $symbols;

    /**
     * @ORM\OneToMany(targetEntity="ArticleAlloy", mappedBy="alloy")
     */
    protected $articles;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

    /**
     * @ORM\Column(type="integer")
     */
    protected $carat;

    /**
     * @ORM\Column(type="float")
     */
    protected $hours;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $enabled;

    private $expectedPrice = null;
    private $available = null;

    public function __construct() {
        $this->description = "";
        $this->symbols = new ArrayCollection();
        $this->articles = new ArrayCollection();
        $this->hours = 1;
        $this->price = 0;
        $this->enabled = false;
    }

    public function __toString() {
        return "$this->name";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Alloy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getLabor()
    {
        return Parameter::$LABOR;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Alloy
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set density
     *
     * @param float $density
     * @return Alloy
     */
    public function setDensity($density)
    {
        return $this;
    }

    /**
     * Get density
     *
     * @return float
     */
    public function getDensity()
    {
        $sum = 0;
        foreach ($this->symbols as $symbol) {
            $sum += $symbol->getQuantity();
        }

        $density = 0;
        foreach ($this->symbols as $symbol) {
            $density += $symbol->getSymbol()->getDensity() * $symbol->getQuantity() / $sum;
        }

        return $density;
    }

    /**
     * Set price per gram
     *
     * @param float $price
     * @return Alloy
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add symbols
     *
     * @param \Gib\WebBundle\Entity\Alloy_Symbol $symbols
     * @return Alloy
     */
    public function addSymbol($symbol)
    {
        $symbol->setAlloy($this);
        $this->symbols[] = $symbol;

        return $this;
    }

    /**
     * Remove symbols
     *
     * @param \Gib\WebBundle\Entity\Alloy_Symbol $symbols
     */
    public function removeSymbol($symbols)
    {
        $this->symbols->removeSymbol($symbols);
    }

    /**
     * Get symbols
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSymbols()
    {
        return $this->symbols;
    }

    /**
     * Add articles
     *
     * @param \Gib\WebBundle\Entity\ArticleAlloy $articles
     * @return Alloy
     */
    public function addArticle(\Gib\WebBundle\Entity\ArticleAlloy $articles)
    {
        $this->articles[] = $articles;

        return $this;
    }

    /**
     * Remove articles
     *
     * @param \Gib\WebBundle\Entity\ArticleAlloy $articles
     */
    public function removeArticle(\Gib\WebBundle\Entity\ArticleAlloy $articles)
    {
        $this->articles->removeSymbol($articles);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Alloy
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Alloy
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set hours
     *
     * @param float $hours
     * @return Alloy
     */
    public function setHours($hours)
    {
        if ($hours < 1) {
            $hours = 1;
        }
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return float
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set id
     *
     * @param string $id
     * @return Alloy
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getExpectedPrice()
    {
        if ($this->expectedPrice === null) {
            $this->available = $this->enabled;

            $this->expectedPrice = 0;

            $sum = 0;

            foreach ($this->symbols as $alloySymbol) {
                $sum += $alloySymbol->getQuantity();
            }

            foreach ($this->symbols as $alloySymbol) {
                $this->expectedPrice += ($alloySymbol->getQuantity() * $alloySymbol->getSymbol()->getPrice())/ $sum;
                $this->available &= $alloySymbol->getSymbol()->isEnabled();
            }


            $this->expectedPrice += $this->hours * $this->getLabor();

            $this->expectedPrice = $this->expectedPrice;
        }

        return $this->expectedPrice;
    }

    public function getRawPrice()
    {
        $this->getExpectedPrice();

        $rawPrice = $this->expectedPrice - $this->getHours() * $this->getLabor();

        return $rawPrice;
    }

    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function isEnabled() {
        return $this->enabled;
    }

    public function isAvailable() {
        $this->getExpectedPrice();
        return $this->available;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->generateId();
        return $this->id;
    }

    public function generateId()
    {
        if ($this->id === null) {
            $this->id = uniqid();
        }
        return $this->id;
    }

    public function generateFilename() {
        return $this->generateId();
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set reference
     *
     * @param integer $reference
     * @return Alloy
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return integer
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate(){
        if ($this->reference == 0) {
            $this->reference = $this->id;
        }
    }

    /**
     * Set carat
     *
     * @param integer $carat
     * @return Alloy
     */
    public function setCarat($carat)
    {
        $this->carat = $carat;

        return $this;
    }

    /**
     * Get carat
     *
     * @return integer
     */
    public function getCarat()
    {
        return $this->carat;
    }
}
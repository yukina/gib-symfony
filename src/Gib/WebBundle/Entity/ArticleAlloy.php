<?php
namespace Gib\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="article_alloy")
 * @ORM\HasLifecycleCallbacks
 */
class ArticleAlloy
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="alloys")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", nullable=false,)
     */
    protected $article;

    /**
     * @ORM\ManyToOne(targetEntity="Alloy", inversedBy="articles")
     * @ORM\JoinColumn(name="alloy_id", referencedColumnName="id", nullable=false)
     */
    protected $alloy;

    /**
     * @ORM\Column(type="float")
     */
    protected $quantity;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $createdAt;

    public function __construct() {
    }

    public function __toString() {
        return "";
        //FIXME
        return "$this->article ($this->alloy)";
    }

    /**
     * @ORM\PrePersist()
     */
    public function preSave() {
        $this->generateId();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return ArticleAlloy
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set article
     *
     * @param \Gib\WebBundle\Entity\Article $article
     * @return ArticleAlloy
     */
    public function setArticle(\Gib\WebBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \Gib\WebBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * Set alloy
     *
     * @param \Gib\WebBundle\Entity\Alloy $alloy
     * @return ArticleAlloy
     */
    public function setAlloy(\Gib\WebBundle\Entity\Alloy $alloy = null)
    {
        $this->alloy = $alloy;

        return $this;
    }

    /**
     * Get alloy
     *
     * @return \Gib\WebBundle\Entity\Alloy 
     */
    public function getAlloy()
    {
        return $this->alloy;
    }

    private function generateId() {
        if ($this->id === null) {
            $this->id = uniqid();
            //$this->id = bin2hex(openssl_random_pseudo_bytes(12));
        }
    }

    /**
     * Set id
     *
     * @param string $id
     * @return ArticleAlloy
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ArticleAlloy
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return ArticleAlloy
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;
    
        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime 
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return ArticleAlloy
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    
        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime 
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
}